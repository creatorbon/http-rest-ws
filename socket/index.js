import * as config from './config';

import { addUser, deleteUser, getUsersInfo, updateUserProgress } from './helpers/usersHelper';
import { addRoom } from './helpers/roomsListHelper';
import { addUserToRoom, updateRoomsAfterLeave } from './helpers/roomHelper';
import { getTextId, gameIsReady } from './helpers/gameHelper';

const usersData = new Map();
const roomsData = new Map();

export default (io) => {
  io.on('connection', (socket) => {
    const { username } = socket.handshake.query;
    const newUser = addUser(usersData, { name: username, id: socket.id });
    io.emit('UPDATE_ROOMS', Array.from(roomsData.values()));

    if (!newUser) {
      socket.emit('ADD_USER_ERROR');
    }

    socket.on('CREATE_ROOM', (roomName) => {
      const newRoom = addRoom(roomsData, {
        name: roomName,
        userId: socket.id,
        maxUsersForRoom: config.MAXIMUM_USERS_FOR_ONE_ROOM,
      });

      if (!newRoom) {
        socket.emit('ADD_ROOM_ERROR');
      } else {
        const currentUserInRoom = roomsData.get(roomName).connectedUsers;
        const usersList = getUsersInfo(usersData, currentUserInRoom);
        io.emit('UPDATE_ROOMS', Array.from(roomsData.values()));
        socket.emit('CONNECT_TO_ROOM', { roomName, usersList });
        socket.join(roomName);
      }
    });

    socket.on('JOIN_ROOM', (roomName) => {
      addUserToRoom(roomsData, roomName, socket.id);
      const currentUserInRoom = roomsData.get(roomName).connectedUsers;
      const usersList = getUsersInfo(usersData, currentUserInRoom);
      socket.join(roomName);
      socket.emit('CONNECT_TO_ROOM', { roomName, usersList });
      io.emit('UPDATE_ROOMS', Array.from(roomsData.values()));
      io.to(roomName).emit('UPDATE_USERS', usersList);
    });

    socket.on('LEAVE_ROOM', (roomName) => {
      const usersList = updateRoomsAfterLeave(usersData, roomsData, roomName, socket.id);

      socket.leave(roomName);
      io.to(roomName).emit('UPDATE_USERS', usersList);
      io.emit('UPDATE_ROOMS', Array.from(roomsData.values()));
    });

    socket.on('CHANGE_READY_STATUS', ({ roomName, userId }) => {
      const currentUser = usersData.get(userId);
      const currenStatus = currentUser.readyStatus;
      usersData.set(userId, { ...currentUser, readyStatus: !currenStatus });
      socket.emit('UPADATE_USER_STATUS', !currenStatus);

      const currentUserInRoom = roomsData.get(roomName).connectedUsers;
      const usersList = getUsersInfo(usersData, currentUserInRoom);
      const gameReadyStatus = usersList.filter((user) => user.readyStatus !== true).length;
      if (!gameReadyStatus) {
        roomsData.get(roomName).gameStart = true;
        gameIsReady(roomsData, roomName, config, io);
      }
      io.emit('UPDATE_ROOMS', Array.from(roomsData.values()));
      io.to(roomName).emit('UPDATE_USERS', usersList);
    });

    socket.on('UPDATE_GAME_DATA', ({ roomName, userId, progress }) => {
      const currentUserInRoom = roomsData.get(roomName).connectedUsers || [];
      updateUserProgress(usersData, userId, progress);
      const usersList = getUsersInfo(usersData, currentUserInRoom);
      io.to(roomName).emit('UPDATE_USERS', usersList);
    });

    socket.on('USER_END_GAME', ({ roomName, userId }) => {
      const currentUser = usersData.get(userId);
      const gameDone = currentUser.progress === 100;
      const currentGame = roomsData.get(roomName);

      if (gameDone) {
        currentGame.winners.push(socket.id);
      }
      if (currentGame.winners.length === currentGame.connectedUsers.length) {
        const usersList = getUsersInfo(usersData, currentGame.winners);
        io.to(roomName).emit('GAME_OVER', usersList);
      }
    });

    socket.on('GAME_TIME_END', (roomName) => {
      const currentUserInRoom = roomsData.get(roomName).connectedUsers;
      const usersList = getUsersInfo(usersData, currentUserInRoom);

      usersList.sort((user, nextUser) => nextUser.progress - user.progress);
      io.to(roomName).emit('GAME_OVER', usersList);
    });

    socket.on('REFRESH_GAME', (roomName) => {
      const currentRoom = roomsData.get(roomName);
      const currentUserInRoom = currentRoom.connectedUsers || [];

      if (currentUserInRoom.length) {
        currentUserInRoom.map((user) => {
          const currentUser = usersData.get(user);
          usersData.set(user, { ...currentUser, readyStatus: false, progress: 0 });
        });
        roomsData.set(roomName, { ...currentRoom, gameStart: false, winners: [] });
      }

      const usersList = getUsersInfo(usersData, currentUserInRoom);
      io.emit('UPDATE_ROOMS', Array.from(roomsData.values()));
      io.to(roomName).emit('UPDATE_ROOM');
      io.to(roomName).emit('UPDATE_USERS', usersList);
    });

    socket.on('disconnect', () => {
      const currentRooms = Array.from(roomsData.values());
      currentRooms.forEach((room) => {
        const connectedUsers = room.connectedUsers;
        const haveUser = connectedUsers || [];

        if (haveUser.indexOf(socket.id) !== -1) {
          const usersList = updateRoomsAfterLeave(usersData, roomsData, room.name, socket.id);
          const gameReadyStatus = usersList.filter((user) => user.readyStatus !== true).length;
          const isUserOverGame = usersList.filter((user) => user.progress === 100);
          const currentRoom = roomsData.get(room.name);

          if (
            currentRoom &&
            isUserOverGame.length &&
            currentRoom.connectedUsers.length === currentRoom.winners.length
          ) {
            const usersList = getUsersInfo(usersData, currentRoom.winners);
            io.to(room.name).emit('GAME_OVER', usersList);
          } else if (currentRoom && !gameReadyStatus && !currentRoom.gameStart) {
            gameIsReady(roomsData, room.name, config, io);
          } else {
          }
          io.to(room.name).emit('UPDATE_USERS', usersList);
        }
      });
      deleteUser(usersData, socket.id);
      io.emit('UPDATE_ROOMS', Array.from(roomsData.values()));
    });
  });
};
