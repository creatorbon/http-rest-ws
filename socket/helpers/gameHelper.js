import { texts } from '../../data';

export const getTextId = () => {
  return Math.floor(Math.random() * texts.length);
};

export const gameIsReady = (roomsMap, roomName, config, io) => {
  const currentRoom = roomsMap.get(roomName);
  if (currentRoom) {
    roomsMap.set(roomName, { ...currentRoom, winners: [] });
    io.to(roomName).emit('GAME_IS_READY', {
      timeToGameStart: config.SECONDS_TIMER_BEFORE_START_GAME,
      timeToGameEnd: config.SECONDS_FOR_GAME,
      textId: getTextId(),
    });
  }
};
