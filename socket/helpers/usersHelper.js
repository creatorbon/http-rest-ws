export const addUser = (usersMap, user) => {
  const username = user.name.trim();
  const haveUser = Array.from(usersMap.values()).filter((el) => el.name === username);
  let addUserStatus = true;

  if (!haveUser.length) {
    usersMap.set(user.id, {
      name: username,
      readyStatus: false,
      progress: 0,
    });
  } else {
    addUserStatus = false;
  }
  return addUserStatus;
};

export const deleteUser = (userMap, userId) => userMap.delete(userId);

export const getUsersInfo = (userMap, usersId) => {
  const usersInfo = [];
  usersId.map((user) => usersInfo.push(userMap.get(user)));
  return usersInfo;
};

export const updateUserProgress = (userMap, userId, progress) => {
  const currentUser = userMap.get(userId);
  userMap.set(userId, { ...currentUser, progress: progress });
};
