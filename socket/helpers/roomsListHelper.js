export const addRoom = (roomsMap, room) => {
  const roomName = room.name.trim();
  const haveRoom = Array.from(roomsMap.values()).filter((el) => el.name === roomName);
  let addRoomStatus = true;

  if (!haveRoom.length && roomName) {
    roomsMap.set(roomName, {
      name: roomName,
      connectedUsers: [room.userId],
      gameStart: false,
      winners: [],
      maxUsersForRoom: room.maxUsersForRoom,
    });
  } else {
    addRoomStatus = false;
  }

  return addRoomStatus;
};
