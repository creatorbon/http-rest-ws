import { addClass, removeClass } from './domHelper.mjs';

const textContainer = document.getElementById('text');
const readyButtom = document.getElementById('ready');
const timer = document.getElementById('timer');

export const gameEndTimer = (timerToGameEnd, roomName, socket) => {
  let timeleft = timerToGameEnd;
  timer.innerHTML = `${timeleft} seconds left`;
  const downloadTimer = setInterval(function () {
    timeleft -= 1;
    if (timeleft <= 0) {
      clearInterval(downloadTimer);
      timer.innerText = '';
      socket.emit('GAME_TIME_END', roomName);
    } else {
      timer.innerHTML = `${timeleft} seconds left`;
    }
  }, 1000);
  return downloadTimer;
};

export const gameStartTimer = (timeToGameStart, timeToGameEnd, textId, socket) => {
  let timeleft = timeToGameStart;
  addClass(readyButtom, 'display-none');
  textContainer.innerHTML = `${timeleft} seconds remaining`;
  const downloadTimer = setInterval(function () {
    timeleft -= 1;
    if (timeleft <= 0) {
      clearInterval(downloadTimer);
      textContainer.innerText = '';
      initGame(textId, timeToGameEnd, socket);
    } else {
      textContainer.innerHTML = `${timeleft} seconds remaining`;
    }
  }, 1000);
};

const getText = async (textId) => {
  const response = await fetch(`/game/texts/${textId}`);
  return await response.text();
};

export const initGame = async (textId, timeToGameEnd, socket) => {
  const roomName = document.getElementById('room-name').innerHTML.trim();
  let string = await getText(textId);

  window.myInterval = gameEndTimer(timeToGameEnd, roomName, socket);

  [...string].forEach((el) => {
    let span = document.createElement('span');
    addClass(span, 'letter');
    span.innerHTML = el;
    textContainer.appendChild(span);
  });

  const letters = document.querySelectorAll('.letter');
  letters[0].classList.add('next');
  let counter = 0;
  let progress = 0;
  const game = (e) => {
    if (e.key === string[counter]) {
      addClass(letters[counter], 'bg-green');
      removeClass(letters[counter], 'next');
      counter++;
      if (counter !== string.length) {
        addClass(letters[counter], 'next');
      }
      progress = (counter / string.length) * 100;
      socket.emit('UPDATE_GAME_DATA', { roomName, userId: socket.id, progress });
    }

    if (counter === string.length) {
      socket.emit('USER_END_GAME', { roomName: roomName, userId: socket.id });
      document.removeEventListener('keydown', game);
    }
  };

  document.addEventListener('keydown', game);
};
