import { createElement, removeClass, addClass } from './domHelper.mjs';

const roomsPage = document.querySelector('#rooms-page');
const gamePage = document.querySelector('#game-page');

export const createRoomPreviw = (room, socket) => {
  const { name, connectedUsers } = room;
  const usersInRoom = connectedUsers.length;
  const roomContainer = createElement({ tagName: 'div', className: 'room-container' });
  const usersCount = createElement({ tagName: 'span' });
  usersCount.innerText = `${usersInRoom} users connected`;

  const roomName = createElement({ tagName: 'h2' });
  roomName.innerText = name;

  const roomButton = createElement({ tagName: 'button' });
  roomButton.innerText = `Join ${name}`;

  const onJoinRoom = () => {
    socket.emit('JOIN_ROOM', name);
  };

  roomButton.addEventListener('click', onJoinRoom);
  roomContainer.append(usersCount, roomName, roomButton);

  return roomContainer;
};

export const updateUsersInRoom = (userList) => {
  const userWrapper = document.getElementById('users-wrapper');
  const updatedUsers = userList.map((user) => createUsersInRoom(user));

  userWrapper.innerHTML = '';
  userWrapper.append(...updatedUsers);
};

const createUsersInRoom = (user) => {
  const username = sessionStorage.getItem('username');
  const userContainer = createElement({ tagName: 'div', className: 'user' });
  const userIndicator = createElement({ tagName: 'span', className: 'user__indicator' });
  const userName = createElement({ tagName: 'h2', className: 'user__name' });
  const userProgressContainer = createElement({ tagName: 'div', className: 'user__progress' });
  const modal = document.getElementById('modal');

  userName.innerText = user.name === username ? `${user.name} (you)` : user.name;
  userProgressContainer.innerHTML = '<div></div>';
  addClass(userProgressContainer.firstChild, 'bg-green');
  addClass(modal, 'display-none');
  modal.innerHTML = '';
  userProgressContainer.firstChild.style.width = `${user.progress}%`;
  if (user.progress === 100) {
    addClass(userProgressContainer.firstChild, 'done');
  } else {
    removeClass(userProgressContainer.firstChild, 'done');
  }
  user.readyStatus ? addClass(userIndicator, 'bg-green') : removeClass(userIndicator, 'bg-green');
  userContainer.append(userIndicator, userName, userProgressContainer);

  return userContainer;
};

export const joinRoom = ({ roomName, usersList }) => {
  const roomNameContainer = document.getElementById('room-name');
  roomNameContainer.innerText = roomName;
  addClass(roomsPage, 'display-none');
  removeClass(gamePage, 'display-none');
  updateUsersInRoom(usersList);
};

export const leaveRoom = () => {
  addClass(gamePage, 'display-none');
  removeClass(roomsPage, 'display-none');
};

export const changeReadyStatus = (status) => {
  const readyButton = document.getElementById('ready');
  readyButton.innerHTML = status ? 'Not Ready' : 'Ready';
};

export const refreshRoom = () => {
  const readyButton = document.getElementById('ready');
  const leaveRoomButtom = document.getElementById('leave-room');
  const text = document.getElementById('text');
  const timer = document.getElementById('timer');
  removeClass(readyButton, 'display-none');
  removeClass(leaveRoomButtom, 'display-none');
  changeReadyStatus(false);
  text.innerHTML = '';
  timer.innerHTML = '';
};
